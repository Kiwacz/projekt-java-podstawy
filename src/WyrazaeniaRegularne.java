import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WyrazaeniaRegularne {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("[^-]*--(\\w+)(--).*");
        Matcher matcher = pattern.matcher("Ala ma kota. Kota ma na imie --Zygmunt--. Kot jest czarny.");
        // boolean matchFound = matcher.find();
        boolean matchFound = matcher.matches();
        if (matchFound) {
            System.out.println(matcher.group(0));
            System.out.println(matcher.group(1));
            System.out.println(matcher.group(2));
        } else {
            System.out.println("Match not found");
        }
    }
}